public class KeyStore01 {
	private static String[] aKeyStore;
	private static int counter = 0;

	public KeyStore01() {
		aKeyStore = new String[100];
	}

	public KeyStore01(int laenge) {
		aKeyStore = new String[laenge];
	}

	public boolean add(String eintrag) {
		if (counter <= 99) {
			aKeyStore[counter] = eintrag;
			counter++;
			return true;
		} else {
			return false;
		}
	}

	public int indexOf(String a) {
		for (int i = 0; i < counter; i++) {
			if (aKeyStore[i].equals(a)) {
				return i;
			}
		}
		return -1;
	}

	public boolean remove(int index) {
		aKeyStore[index] = null;
		for (int i = 0; i < aKeyStore.length; i++) {
			if (i == index) {
				// shifting elements
				for (int j = i; j < aKeyStore.length - 1; j++) {
					aKeyStore[j] = aKeyStore[j + 1];
				}
				aKeyStore[aKeyStore.length - 1] = "";
				counter--;
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String ergebnis = "";
		for (int i = 0; i < counter; i++) {
			int KeyNummber = i + 1;
			ergebnis = ergebnis + " Schlüsselnummer: " + KeyNummber + " = " + aKeyStore[i] + "";
		}
		return "(" + ergebnis + ")";
	}

}
