import java.util.ArrayList;

public class KeyStore02 {
	ArrayList<String> Liste = new ArrayList<String>();

	public boolean add(String eingabe) {
		Liste.add(eingabe);
		return true;
	}

	public int indexOf(String eingabe) {
		if (Liste.contains(eingabe))
			return Liste.indexOf(eingabe);
		return -1;
	}
	public void remove(int index) {
		Liste.remove(index);
	}

	@Override
	public String toString() {
		return "KeyStore02 [Liste=" + Liste +  "]";
	}
	
}
