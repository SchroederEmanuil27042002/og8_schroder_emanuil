package schleifen;

import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl;
		
		System.out.println("Gib bitte eine Zahl ein");
		 anzahl = scan.nextInt();
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}
		int i= 0;
		// Minusschleife
		while (i < anzahl) {
			System.out.println("-");
			i++;
		}
		// Malzeichen
		i = 0;
		do {
			System.out.println("*");
			i++;
		} while (i < anzahl);
	}

}
