package quicksort;

public class QuicksortTest {

	public static int[] a = { 9, 44, 55, 21, 224, 33, 7676, 544, 1, 33 };

	public int[] quicksort(int b, int c) {
		int d;
		if (b < c) {
			d = listenaufteilung(b, c);
			quicksort(b, c - 1);
			quicksort(d + 1, c);
		}
		return a;
	}

	int listenaufteilung(int b, int c) {
		int i, j, x = a[(b + c) / 2];
		i = b - 1;
		j = c + 1;
		while (true) {
			do {
				i++;
			} while (a[i] < x);

			do {
				j--;
			} while (a[j] > x);

			if (i < j) {
				int k = a[i];
				a[i] = a[j];
				a[j] = k;
			} else {
				return j;
			}
		}
	}

	public static void main(String[] args) {
		QuicksortTest qs = new QuicksortTest();
		int[] b = qs.quicksort(0, a.length - 1);
		for (int j = 0; j < b.length; j++) {
			System.out.println(j + ": "+ b[j]);
		}
		
	}

}
