//in Zusammenarbeit mit Alexander Holz (Emanuil Schr�der brauchte Hilfe)
import java.util.Scanner;

public class BerechnePrimzahl {
	private static boolean istPrim;

	public static void main(String[] args) {
		Stopuhr uhr = new Stopuhr();
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("Bitte gib dein Wert ein");
			istPrim = true;
			long eingabe = scan.nextLong();
			uhr.start();
			if (eingabe > 1) {

				for (long i = 2; i < eingabe; i++) {
					if (eingabe % i == 0) {

						istPrim = false;
						break;

					}

				}
			} else {
				istPrim = false;
			}
			uhr.stop();
			if (istPrim) {
				System.out.println("Deine Eingabe " + eingabe + " ist eine Primzahl ");
			} else {
				System.out.println("Deine Eingabe " + eingabe + " ist keine Primzahl");
			}	
			System.out.println("Deine Zeit betr�gt " + uhr.getDauerinMS());

		}
	}
}