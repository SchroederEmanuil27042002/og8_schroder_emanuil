public class Stopuhr {

	private long startzeit;
	private long stopzeit;
	
	public void start(){
	startzeit = System.currentTimeMillis();	
	}
	public void stop(){
	stopzeit = System.currentTimeMillis();
	}
	public void zuruecksetzen() {
		startzeit=0;
		stopzeit=0;
	}
	public long getDauerinMS(){
	return stopzeit - startzeit;	
	}
}