package osz_kickers;



public abstract class Mitglied {
	
	//Attribute
	private String name;
	private int telefonnummer;
	private boolean jahresbeitragBezahlt;
	
	//Konstruktoren
	public Mitglied() {
		super();
	}
	public Mitglied(String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}
	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}
	
}
