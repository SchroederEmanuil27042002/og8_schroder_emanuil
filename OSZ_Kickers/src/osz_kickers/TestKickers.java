package osz_kickers;


public class TestKickers {

	public static void main(String[] args) {
		//Erstellung
		Trainer trainer1 = new Trainer('A',200.0,"Lappen",1245654,true);
		MannschaftsLeiter mannschaftsLeiter1 = new MannschaftsLeiter ("Tornado Kickers 1996", 40, 2, "Sturm", "Philip Schnell", 4524261, true);
		EhrenamtlicherSchiedsrichter ehrenamtlicherSchiedsrichter1 = new EhrenamtlicherSchiedsrichter(5, "Mueller", 4237426, true);
		Spieler spieler1 = new Spieler(7,"Verteidigung", "Boateng Musterman", 5312456, true);
		Mannschaft mannschaft1 = new Mannschaft("Tornado Kickers 1996", "Regional-Liga");
		Spiel spiel1 = new Spiel("27.09.2018 18:00Uhr","Heim",3,2,new String[] {"Philipp Schnell"},new String[] {"Boateng Musterman"});
		
		//Ausgabe
		
		//Trainer
		trainer1.setLizenzKlasse('D');
		System.out.println("Lizenz-Klasse: " + trainer1.getLizenzKlasse());
		trainer1.setAufwandEntschaedigung(500.0);
		System.out.println("Aufwand Entschaedigung: " + trainer1.getAufwandEntschaedigung() + " �");
		System.out.println(trainer1);
		System.out.println();
		
		//Mannschafts-Leiter
		mannschaftsLeiter1.setRabattAufJahresbeitragProzent(55);
		System.out.println("Rabbat auf Jahresbeitrag: " + mannschaftsLeiter1.getRabattAufJahresbeitragProzent() + " %");
		System.out.println(mannschaftsLeiter1);
		System.out.println();
		
		//Ehrenamtlicher Schiedsrichter
		System.out.println(ehrenamtlicherSchiedsrichter1);
		System.out.println();
		
		//Spieler
		System.out.println(spieler1);
		System.out.println();
		
		//Mannschaft
		System.out.println(mannschaft1);
		System.out.println();
		
		//Spiel
		System.out.println(spiel1);
	}
}
