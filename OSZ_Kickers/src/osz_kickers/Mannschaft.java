package osz_kickers;

public class Mannschaft {
	
	//Attribute
	private String nameMannschaft;
	private String spielKlasse;
	
	//Konstruktoren
	public Mannschaft() {
		super();
	}
	public Mannschaft(String nameMannschaft, String spielKlasse) {
		super();
		this.nameMannschaft = nameMannschaft;
		this.spielKlasse = spielKlasse;
	}
	
	//Methoden
	@Override
	public String toString() {
		return "Mannschaft [Name der Mannschaft:" + nameMannschaft + ", Spiel-Klasse:" + spielKlasse + "]";
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public String getSpielKlasse() {
		return spielKlasse;
	}
	public void setSpielKlasse(String spielKlasse) {
		this.spielKlasse = spielKlasse;
	}
	
}
