package osz_kickers;



public class Spieler extends Mitglied{
	
	//Attribute
	private int trikotNummer;
	private String spielPosition;
	
	//Konstruktoren
	public Spieler() {
		super();
	}

	public Spieler(int trikotNummer, String spielPosition, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.trikotNummer = trikotNummer;
		this.spielPosition = spielPosition;
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public int getTrikotNummer() {
		return trikotNummer;
	}

	public void setTrikotNummer(int trikotNummer) {
		this.trikotNummer = trikotNummer;
	}

	public String getSpielPosition() {
		return spielPosition;
	}

	public void setSpielPosition(String spielPosition) {
		this.spielPosition = spielPosition;
	}

	//Methoden
	@Override
	public String toString() {
		return "Spieler [Trikot-Nummer: " + trikotNummer + ", Spiel-Position: " + spielPosition + ", Name: " + this.getName() + ", Telefonnummer: " + this.getTelefonnummer() + ", Jahresbeitrag bezahlt: " + this.isJahresbeitragBezahlt() + "]";
	}
	
	
}
