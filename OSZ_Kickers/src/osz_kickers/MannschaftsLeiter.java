package osz_kickers;


public class MannschaftsLeiter extends Spieler{
	
	//Attribute
	private String nameMannschaft;
	private int rabattAufJahresbeitragProzent;
	
	//Konstruktoren
	public MannschaftsLeiter() {
		super();
	}
	public MannschaftsLeiter(String nameMannschaft, int rabattAufJahresbeitragProzent, int trikotNummer, String spielPosition, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(trikotNummer, spielPosition, name, telefonnummer, jahresbeitragBezahlt);
		this.nameMannschaft = nameMannschaft;
		this.rabattAufJahresbeitragProzent = rabattAufJahresbeitragProzent;
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getRabattAufJahresbeitragProzent() {
		return rabattAufJahresbeitragProzent;
	}
	public void setRabattAufJahresbeitragProzent(int rabattAufJahresbeitragProzent) {
		if (rabattAufJahresbeitragProzent <= 50) {
			this.rabattAufJahresbeitragProzent = rabattAufJahresbeitragProzent;	
		}
		else {
			System.out.println("Sie haben einen zu hohen Rabatt eingegeben!");
		}
	}
	
	//Methoden
	@Override
	public String toString() {
		return "MannschaftsLeiter [Name der Mannschaft: " + nameMannschaft + ", Rabatt auf Jahresbeitrag in Prozent: " + rabattAufJahresbeitragProzent + "% , Trikot-Nummer: " + this.getTrikotNummer() + ", Spiel-Position: " + this.getSpielPosition() + ", Name: " + this.getName() + ", Telefonnummer: " + this.getTelefonnummer() + ", Jahresbeitrag bezahlt: " + this.isJahresbeitragBezahlt() + "]";
	}
	

}
