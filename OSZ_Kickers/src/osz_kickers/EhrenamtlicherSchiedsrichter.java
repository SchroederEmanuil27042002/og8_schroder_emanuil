package osz_kickers;


public class EhrenamtlicherSchiedsrichter extends Mitglied{
	
	
	private int anzahlSpiele;
	
	//Konstruktoren
	public EhrenamtlicherSchiedsrichter() {
		super();
	}

	public EhrenamtlicherSchiedsrichter(int anzahlSpiele, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.anzahlSpiele = anzahlSpiele;
	}

	//Verwaltungsmethoden (Getter und Setter)
	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}

	//Methoden
	@Override
	public String toString() {
		return "Ehrenamtlicher-Schiedsrichter [anzahlSpiele: " + anzahlSpiele + ", Name: " + this.getName() + ", Telefonnummer: " + this.getTelefonnummer() + ", Jahresbeitrag bezahlt: " + this.isJahresbeitragBezahlt() + "]";
	}
}
