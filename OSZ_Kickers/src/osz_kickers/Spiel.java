package osz_kickers;

import java.util.Arrays;

public class Spiel {
	
	//Attribute
	private String zeitpunkt;
	private String mannschaftTyp;
	private int gastTore;
	private int heimTore;
	private String[] gelbeKarte;
	private String[] roteKarte;
	
	//Konstruktoren
	public Spiel() {
		super();
	}
	public Spiel(String zeitpunkt, String mannschaftTyp, int gastTore, int heimTore, String[] gelbeKarte,
			String[] roteKarte) {
		super();
		this.zeitpunkt = zeitpunkt;
		this.mannschaftTyp = mannschaftTyp;
		this.gastTore = gastTore;
		this.heimTore = heimTore;
		this.gelbeKarte = gelbeKarte;
		this.roteKarte = roteKarte;
	}
	
	//Methoden
	@Override
	public String toString() {
		return "Spiel [Zeitpunkt des Spiels:" + zeitpunkt + ", Mannschaft-Typ:" + mannschaftTyp + ", Gast-Tore:" + gastTore
				+ ", Heim-Tore:" + heimTore + ", Gelbe-Karten:" + Arrays.toString(gelbeKarte) + ", Rote-Karten:"
				+ Arrays.toString(roteKarte) + "]";
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public String getZeitpunkt() {
		return zeitpunkt;
	}
	public void setZeitpunkt(String zeitpunkt) {
		this.zeitpunkt = zeitpunkt;
	}
	public String getMannschaftTyp() {
		return mannschaftTyp;
	}
	public void setMannschaftTyp(String mannschaftTyp) {
		this.mannschaftTyp = mannschaftTyp;
	}
	public int getGastTore() {
		return gastTore;
	}
	public void setGastTore(int gastTore) {
		this.gastTore = gastTore;
	}
	public int getHeimTore() {
		return heimTore;
	}
	public void setHeimTore(int heimTore) {
		this.heimTore = heimTore;
	}
	public String[] getGelbeKarte() {
		return gelbeKarte;
	}
	public void setGelbeKarte(String[] gelbeKarte) {
		this.gelbeKarte = gelbeKarte;
	}
	public String[] getRoteKarte() {
		return roteKarte;
	}
	public void setRoteKarte(String[] roteKarte) {
		this.roteKarte = roteKarte;
	}
	
}
