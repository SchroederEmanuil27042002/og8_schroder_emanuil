package osz_kickers;



public class Trainer extends Mitglied{

	//Attribute
	private char lizenzKlasse;
	private double aufwandEntschaedigung;
	
	//Konstruktoren
	public Trainer() {
		super();
	}
	public Trainer(char lizenzKlasse, double aufwandEntschaedigung, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.lizenzKlasse = lizenzKlasse;
		this.aufwandEntschaedigung = aufwandEntschaedigung;
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public char getLizenzKlasse() {
		return lizenzKlasse;
	}
	public void setLizenzKlasse(char lizenzKlasse) {
		if (lizenzKlasse == 'A' || lizenzKlasse == 'B' || lizenzKlasse == 'C') {
			this.lizenzKlasse = lizenzKlasse;
		}
		else {
			System.out.println("Es gibt nur die folgenden LizenzKlassen: A, B, C  !");
		}
	}
	public double getAufwandEntschaedigung() {
		return aufwandEntschaedigung;
	}
	public void setAufwandEntschaedigung(double aufwandEntschaedigung) {
		if (aufwandEntschaedigung >= 125.00 && aufwandEntschaedigung <= 450.00) {
			this.aufwandEntschaedigung = aufwandEntschaedigung;
		}
		else {
			System.out.println("Sie haben eine zu hohe oder zu niedrige Aufwand Entschaedigung eingegeben!");
		}
	}
	
	//Methoden
	@Override
	public String toString() {
		return "Trainer [Lizenz-Klasse: " + lizenzKlasse + ", Aufwand Entschaedigung: " + aufwandEntschaedigung + ", Name: " + this.getName() + ", Telefonnummer: " + this.getTelefonnummer() + ", Jahresbeitrag bezahlt: " + this.isJahresbeitragBezahlt() + "]";
	}
	
		
	
}
