
public class BinaereSuche {
	int binaere_Suche(int A[], int eingabe, int gesuchteZahl) {
		int NICHT_GEFUNDEN = -1;
		int left = 0;
		int right = eingabe - 1;

		while (left <= right) {
			int mitte = left + ((right - left) / 2);

			if (A[mitte] == gesuchteZahl)
				return mitte;
			else if (A[mitte] > gesuchteZahl)
				right = mitte - 1;
			else
				left = mitte + 1;
		}

		return NICHT_GEFUNDEN;

	}
	
}
