import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class einMioPrim {

	private File file;

	public einMioPrim(File file) {
		this.file = file;
	}

	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);

			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("nicht verf�gbar");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		File file = new File("1 Mio. PrimzahlenNeu.txt");
		DateinVerwalter dv = new DateinVerwalter(file);

		long i  = 1;
		boolean Prim = true;
		while (i < 999_999_999) { 
			for (long j = 2; j < i - 1; j++) {

				if (i % j == 0) {

					Prim = false;

				}

			}

			if (Prim) {

				dv.schreibe(i+"");
				

			} else {

				Prim = true;

			}

			i++;
			
		}
	}

}
