import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DateinVerwalter {
	private File file;

	public DateinVerwalter(File file) {
		this.file = file;
	}

	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (IOException e) {
			System.out.println("Nicht vorhanden");
			e.printStackTrace();
		}
	}

	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);

			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("nicht verf�gbar");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		File file = new File("OG8.txt");
		DateinVerwalter dv = new DateinVerwalter(file);
		dv.schreibe("tommy");
	}

}
