/* 01 */   public class TestGehege { 
/* 02 */    public static void main(String args) {
/* 03 */     // Erstellen des Objektes 
/* 04 */     Gehege meinGehege = new Gehege(); 
/* 05 */ // Setzen der Werte f�r das Gehege 
/* 06 */     double flaeche = 23.0;
/* 07 */     meinGehege.setFlaeche(flaeche); 
/* 08 */     meinGehege.setPreis("30000"); 
/* 09 */     meinGehege.setAttraktivitaet(Attraktivitaet); 
/* 10 */     // Meine Affen 
/* 11 */     affe a1 = new Affe("Bert"); 
/* 12 */     affe a2 = new Affe("Berta"); 
/* 13 */     Affe[] meineAffen = new Affe[10]; 
/* 14 */     meineAffen[0] = a1; 
/* 15 */     meineAffen[1] = a2; 
/* 16 */     // Alter setzen 
/* 17 */     meineAffen[0].setAlter(14.0); 
/* 18 */     meineAffen[1].setAlter(12);
/* 19 */     // Affen ins Gehege setzen
/* 20 */     meinGehege.setListAffen(meineAffen);
/* 21 */     // Berta bricht aus! 
/* 22 */     a2.setGehege(null); 
/* 23 */     // Schaue nach, ob Berta im richtigen Gehege ist 
/* 24 */     if (a2.getGehege() ==meinGehege) { 
/* 25 */       System.out.println("Berta ist im Gehege.");
/* 26 */      } else  
/* 27 */       System.out.println("Berta ist irgendwo anders!"); 
/* 28 */      
/* 29 */     // Wer ist denn so im Gehege?
/* 30 */     for (int i = 0; i <= meinGehege.getListAffen().length; i++) {
/* 31 */      if (meinGehege.getListAffen() [i] != null) 
/* 32 */       System.out.println("Affe Nr. " + i + 1 + ": " +
/* 33 */        	meinGehege.getListAffen()[i].getName()); 
/* 34 */      }
/* 35 */     } 
/* 36 */   } 