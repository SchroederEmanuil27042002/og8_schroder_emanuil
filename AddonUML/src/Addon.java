
public class Addon {
	private String name;
	private double preis;
	private int idNummer;
	private int maxAnzahl;
	private int tatsaelicheAnzahl;

	public Addon(String name, double preis, int idNummer, int maxAnzahl, int tatsaelicheAnzahl) {
		this.name=name;
		this.preis=preis;
		this.idNummer=idNummer;
		this.maxAnzahl=maxAnzahl;
		this.tatsaelicheAnzahl=tatsaelicheAnzahl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getidNummer() {
		return idNummer;
	}

	public void setidNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public int getmaxAnzahl() {
		return maxAnzahl;
	}

	public void setmaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}

	public int gettatsaelicheAnzahl() {
		return tatsaelicheAnzahl;
	}

	public void settatsaelicheAnzahl(int tatsaelicheAnzahl) {
		this.tatsaelicheAnzahl = tatsaelicheAnzahl;
	}

	public int verbrauchen() {

		return 0;
	}

	public int kaufen() {

		return 0;
	}

	public int bestandswert() {

		return 0;
	}
}
