package git_taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Dividieren     |");
		System.out.println("|        4. Multiplizieren |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println("Gib deinen erste Zahl ein");
			double zahl1 = myScanner.nextDouble();
			System.out.println("Gib deine zweite Zahl ein");
			double zahl2 = myScanner.nextDouble();

			System.out.println("ertste Zahl + zweite Zahl = " + ts.add(zahl1, zahl2));
			break;

		case '2':
			System.out.println("Gib deinen erste Zahl ein");
			zahl1 = myScanner.nextDouble();
			System.out.println("Gib deine zweite Zahl ein");
			zahl2 = myScanner.nextDouble();

			System.out.println("ertste Zahl + zweite Zahl = " + ts.sub(zahl1, zahl2));
			break;

		case '3':
			System.out.println("Gib deinen erste Zahl ein");
			zahl1 = myScanner.nextDouble();
			System.out.println("Gib deine zweite Zahl ein");
			zahl2 = myScanner.nextDouble();

			System.out.println("ertste Zahl + zweite Zahl = " + ts.div(zahl1, zahl2));
			break;

		case '4':
			System.out.println("Gib deinen erste Zahl ein");
			zahl1 = myScanner.nextDouble();
			System.out.println("Gib deine zweite Zahl ein");
			zahl2 = myScanner.nextDouble();

			System.out.println("ertste Zahl + zweite Zahl = " + ts.mul(zahl1, zahl2));
			break;
			
		case '5':
			System.exit(0);

		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

	}

}