import java.util.Arrays;

public class selectsortübungtest {
	static int[] zahlen = { 14, 4, 95, 33, 558, 7788, 5 };

	public static int[] Selectsort(int[] a) {
		for (int i = 0; i < a.length - 1; i++) {
			System.out.println(Arrays.toString(a));
			int kleinste = i;
			for (int j = i; j < a.length; j++) {
				if (a[j] < a[kleinste]) {
					kleinste = j;
				}
			}
			int temp = a[i];
			a[i] = a[kleinste];
			a[kleinste] = temp;

		}
		return a;
	}

	public static void main(String[] a) {
		System.out.println(Arrays.toString(Selectsort(zahlen)));
	}
}
