import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.ArrayList;

//Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den Daten eine Liste an Kind-Objekte anlegen
public class KindGenerator {
	ArrayList<Kind> KinderListe = new ArrayList<Kind>();
	private File file;

	public KindGenerator  (File file) {
		super();
		this.file = file;
	}

	public ArrayList<Kind> KinderListe() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				Kind kind = kindhinzu(s);
				KinderListe.add(kind);
			}
		} catch (IOException e) {
			System.out.println("Nicht vorhanden");
			e.printStackTrace();
		}
		return KinderListe;
	}

	public Kind kindhinzu(String a) {
		String s = a.replace(",", "");
		String[] liste = s.split(" ");
		for (int i = 5; i < liste.length; i++) {
			liste[4] += liste[i];
		}
		return new Kind(liste[0], liste[1], liste[2], liste[4], Integer.parseInt(liste[3]));
	}

	/*
	 * public int[] kindhinzu() { int[] kindliste = new int[10_000]; for (int i = 0;
	 * i < kindliste.length; i++) {
	 * 
	 * } }
	 */
}
