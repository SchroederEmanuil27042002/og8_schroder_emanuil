package omnom;

public class Haustier {
	private int hunger = 100;
	private int muede = 100;
	private int zufrieden = 100;
	private int gesund = 100;
	private String name;

	public Haustier() {

	}

	public Haustier(String name) {
		super();
		this.name = name;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger < 0 || hunger > 100) {

		} else
			this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede < 0 || muede > 100) {

		} else

			this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden < 0 || zufrieden > 100) {

		} else

			this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund < 0 || gesund > 100) {

		} else

			this.gesund = gesund;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public void fuettern(int anzahl) {
		this.setHunger(this.getHunger() + anzahl);
	}

	public void schlafen(int dauer) {
		this.setMuede(this.getMuede() + dauer);
	}

	public void spielen(int dauer) {
		this.setZufrieden(this.getZufrieden() + dauer);
	}

	public void heilen() {
		setGesund(100);
	}

}
