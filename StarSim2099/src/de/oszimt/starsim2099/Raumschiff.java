package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	double posX;
	double posY;
	int maxKapazitaet;
	String typ;
	String antrieb;
	int winkel;

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMaxKapazitaet() {
		return maxKapazitaet;
	}

	public void setMaxKapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}

}
