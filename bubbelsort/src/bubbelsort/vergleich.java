package bubbelsort;

import java.util.Arrays;

public class vergleich {

	public static int[] bubbelsortvergleich(int[] a) {
		for (int n = a.length; n > 1; n--) {
			for (int i = 0; i < n-1; i++) {
				if (a[i] > a[i + 1]) {
					int temp = a[i + 1];
					a[i + 1] = a[i];
					a[i] = temp;
					
				}
			}
			System.out.println(Arrays.toString(a));
		}
		return a;

	}
}
